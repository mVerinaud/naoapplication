$(document).ready(function ()
{
    //Mise en place du tableau de tri
    if (document.getElementById("tableau") != null)
    {
        $('#tableau').DataTable(
        {
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "ajax" : ajax,
            "sAjaxDataProp": "data",
            "pageLength": 25,
            "columns":[
                {"data": "famille"},
                {"data": "rang"},
                {"data": "nomLatin"},
                {"data": "nomFrancais"},
                {"data": "habitat"},
                {"data": "cdNom"}
            ],
            "order": [[ 2, "asc" ]],
            "language":
            {
                "sProcessing": "Traitement en cours...",
                "sSearch": "Rechercher&nbsp;:",
                "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix": "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate":
                {
                    "sFirst": "Premier",
                    "sPrevious": "Pr&eacute;c&eacute;dent",
                    "sNext": "Suivant",
                    "sLast": "Dernier"
                },
                "oAria":
                {
                    "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            },
            "columnDefs":
            [
                {
                    "targets": [5],
                    "sorting": false
                }
            ]
        });
    }
});

function initMap()
{
    mapMonde = new google.maps.Map(document.getElementById("mapMonde"),
        {
            center: {lat: 0, lng: 0},
            zoom: 2
        });
    initMarqueursMonde(mapMonde, metropole, guyane, martinique, guadeloupe, saintMartin, saintBarthelemy, saintPierre, mayotte, ilesEparses, reunion, taaf, polynesie, nouvelleCaledonie, wallis, clipperton);

    if (document.getElementById("mapObservations") != null)
    {
        mapObservations = new google.maps.Map(document.getElementById("mapObservations"),
            {
                center: {lat: 46.58302876545447, lng: 2.61474609375},
                zoom: 5
            });
        initMarqueursObservations(mapObservations, observationsEnregistrees);
    }
}

function initMarqueursMonde(mapMonde, metropole, guyane, martinique, guadeloupe, saintMartin, saintBarthelemy, saintPierre, mayotte, ilesEparses, reunion, taaf, polynesie, nouvelleCaledonie, wallis, clipperton)
{
    if (metropole != "")
    {
        initMarqueur(mapMonde, metropole, 48.862725, 2.287592000000018);
    }
    if (guyane != "")
    {
        initMarqueur(mapMonde, guyane, 3.933888999999999, -53.125782000000015);
    }
    if (martinique != "")
    {
        initMarqueur(mapMonde, martinique, 14.641528, -61.024174000000016);
    }
    if (guadeloupe != "")
    {
        initMarqueur(mapMonde, guadeloupe, 16.265, -61.55099999999999);
    }
    if (saintMartin != "")
    {
        initMarqueur(mapMonde, saintMartin, 18.0708298, -63.05008090000001);
    }
    if (saintBarthelemy != "")
    {
        initMarqueur(mapMonde, saintBarthelemy, 17.9, -62.83333300000004);
    }
    if (saintPierre != "")
    {
        initMarqueur(mapMonde, saintPierre, 46.8852, -56.3159);
    }
    if (mayotte != "")
    {
        initMarqueur(mapMonde, mayotte, -12.8275, 45.166244000000006);
    }
    if (ilesEparses != "")
    {
        initMarqueur(mapMonde, ilesEparses, -22.343889, 40.367222);
    }
    if (reunion != "")
    {
        initMarqueur(mapMonde, reunion, -21.115141, 55.536384);
    }
    if (taaf != "")
    {
        initMarqueur(mapMonde, taaf, -49.280366, 69.34855700000003);
    }
    if (polynesie != "")
    {
        initMarqueur(mapMonde, polynesie, -17.679742, -149.40684299999998);
    }
    if (nouvelleCaledonie != "")
    {
        initMarqueur(mapMonde, nouvelleCaledonie, -20.904305, 165.61804200000006);
    }
    if (wallis != "")
    {
        initMarqueur(mapMonde, wallis, -14.2938, -178.11649999999997);
    }
    if (clipperton != "")
    {
        initMarqueur(mapMonde, clipperton, 10.2833333, -109.21666670000002);
    }
}

function initMarqueur(map, localisation, lat, lng)
{
    new google.maps.Marker(
    {
        position: {lat: lat, lng: lng},
        map: map,
        label: localisation
    });
}

function initMarqueursObservations(mapObservations, observations)
{
    observations.forEach(function (observation)
    {
        var coords = observation.coordonnees.split(",");
        var latitude = parseFloat(coords[0]);
        var longitude = parseFloat(coords[1]);
        var info = new google.maps.InfoWindow(
            {
                content: 'Enregistré par ' + observation.auteur + ', le ' + observation.date
            }
        );
        var marqueur = new google.maps.Marker(
            {
                position : {lat: latitude,lng: longitude},
                map: mapObservations
            }
        );
        marqueur.addListener('click', function()
        {
            info.open(mapObservations, marqueur);
        });
        mapObservations.setCenter({lat: latitude, lng: longitude});
    });

}