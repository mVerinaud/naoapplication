$(document).ready(function () {
        if ($("#tableauObs").length) {
            $('#tableauObs').DataTable(
                    {
                        responsive: true,
                        language:
                                {
                                    "sProcessing": "Traitement en cours...",
                                    "sSearch": "Rechercher&nbsp;:",
                                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                                    "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                                    "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                                    "sInfoPostFix": "",
                                    "sLoadingRecords": "Chargement en cours...",
                                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                                    "oPaginate":
                                            {
                                                "sFirst": "Premier",
                                                "sPrevious": "Pr&eacute;c&eacute;dent",
                                                "sNext": "Suivant",
                                                "sLast": "Dernier"
                                            },
                                    "oAria":
                                            {
                                                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                                                "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                                            }
                                }

                    });
        }
    }
    );



    /* Fonction d'initialisation de la map appelée au chargement de la page  */
    var map;
    var geocoder;
    var infowindow;
    var marker;
    var latlng;


    function tronqueLatLong(latlong, from)
    {
        var arrayLatLong = latlong.split(",", 2);
        var lat = arrayLatLong[0];
        var long = arrayLatLong[1];
        var arrayLat = lat.split(".", 2);
        var arrayLong = long.split(".", 2);
        var lat2 = arrayLat[1].substr(0, 1);
        var long2 = arrayLong[1].substr(0, 1);
        if (from === "input") {
            return arrayLat[0] + "." + lat2 + ", " + arrayLong[0] + "." + long2;
        } else if (from === "google") {
            return arrayLat[0] + "." + lat2 + "," + arrayLong[0] + "." + long2;
        }
    }

    $("#observation").submit(function (event) {
        event.preventDefault();

        var coordonnees = $("#observation_coordonnees").val();

        var address = $("#observation_adresse").val();
        var result = address.indexOf('Unnamed Road,');
        if (result > -1) {

            var address = address.split("Unnamed Road,", 2);
            var address = address[1];

        }
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var coords = results[0].geometry.location;
                var strco = String(coords).split("(", 2);

                if (tronqueLatLong(String(coordonnees), "input") === tronqueLatLong(strco[1]), "google")
                {
                    $("#observation").submit();
                } else {
                    alert("Les coordonnées GPS ne correpondent pas à l'adresse.");

                }


            } else {
                alert("Le geocodage n\'a pu etre effectue pour la raison suivante: " + status);
            }
        });

    });





    function ajouteObservation() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(46.8566667, 2.3509871);
        var myOptions = {zoom: 6, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP};
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        infowindow = new google.maps.InfoWindow();
        marker = new google.maps.Marker();
    }

    function afficheObservation() {
        var latlng = getCoordonnees();
        var myOptions = {zoom: 8, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP};
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        infowindow = new google.maps.InfoWindow();
        marker = new google.maps.Marker();
        marker.setPosition(latlng);
        marker.setMap(map);
        marker.setTitle('Ici');
    }


    function nouveauMarker(valeur, adresse)
    {
        map.setZoom(11);
        map.setCenter(valeur);
        marker.setPosition(valeur);
        marker.setMap(map);
        infowindow.setContent(adresse);
        infowindow.open(map, marker);

    }
    /* geolocalisation  */
    function geolocalise()
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {lat: position.coords.latitude, lng: position.coords.longitude};
                marker.setPosition(pos);
                marker.setMap(map);
                marker.setTitle('Vous êtes ici');
                map.setCenter(pos);
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                geocoder.geocode({'latLng': latlng}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            map.setZoom(11);
                            $('#observation_adresse').val(results[0].formatted_address);
                            $('#observation_coordonnees').val(position.coords.latitude + ',' + position.coords.longitude);
                        }
                    }
                });
            }, function () {
                handleLocationError(true, marker, map.getCenter());
            }, {enableHighAccuracy: true});
        } else {
            handleLocationError(false, marker, map.getCenter());
        }
    }

    /* erreur geolocalisation  */
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                'Erreur: Le service de géolocalisation est désactivé.' :
                'Erreur: Votre navigateur ne supporte pas la géolocalisation.');
    }


    /* rechercher par adresse sur la map  */
    function rechercheParAdresse() {
        var address = $("#observation_adresse").val();
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var coords = results[0].geometry.location;
                nouveauMarker(coords, results[0].formatted_address);
                $('#observation_coordonnees').val(coords.lat() + ',' + coords.lng());
                $('#observation_adresse').val(results[0].formatted_address);

            } else {
                alert("Le geocodage n\'a pu etre effectue pour la raison suivante: " + status);
            }
        });

    }

    /* Fonction de géocodage inversé (en fonction des coordonnées de l'adresse)  */
    function rechercheParCoordonnees() {
        var input = $("#observation_coordonnees").val();
        var latlngStr = input.split(",", 2);
        var latlng = new google.maps.LatLng(parseFloat(latlngStr[0]), parseFloat(latlngStr[1]));
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    nouveauMarker(latlng, results[0].formatted_address);
                    $('#observation_adresse').val(results[0].formatted_address);
                }
            } else {
                alert("Le geocodage n\'a pu etre effectue pour la raison suivante: " + status);
            }
        });
    }
