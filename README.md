L’application de l'association NAO permet : 

 - De rechercher les différentes espèces d’oiseaux parmi la base de données 
(téléchargement via la base de données TAXREF du Muséum National d’Histoire 
Naturelle, classe « Aves ») 
 - De les afﬁcher sur une carte (après ﬁltre par espèce) 
 - De saisir une « observation » d’un oiseau sur le terrain, avec nom, date, 
coordonnées GPS et photo facultative. 
 - De valider les observations des particuliers (uniquement avec un compte 
naturaliste). 

Développé par CMDL sous licence OC