<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class UserController extends Controller
{
    /**
     * @Route("/login", name="login" , schemes={"%la_voie_cryptee%"})
     * @Template("user/login.html.twig")
     */
    public function loginAction()
    {
        // identification d'un utilisateur
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        if ($error){ $error = 'erreur sur le pseudo ou le mot de passe';}
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return  array(
            'last_username' => $lastUsername,
            'error'         => $error,
        );
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        // déconnection d'un utilisateur
    }

    /**
     * @Route("/activation/{essai}", name="active" , schemes={"%la_voie_cryptee%"})
     * @Template("user/active.html.twig")
     */
    public function activeAction($essai, Request $request)
    {
        $activ = $this->container->get('user.registration')->activation($essai, $request);

        if ($activ[0]->isValid() && $activ[1]){
            return $this->redirectToRoute('login');
        }elseif ($activ[0]->isValid() && !$activ[1] && $activ[2]> 8 ){
            return $this->redirectToRoute('nao_home');
         }elseif ($activ[0]->isValid() && !$activ[1]){
            return $this->redirectToRoute('active',['essai' => $activ[2] ]);
        }

        return  array( 'form' => $activ[0]->createView(),
            'essai'  => $essai
        );
    }

    /**
     * @Route("/oubli", name="oubli", schemes={"%la_voie_cryptee%"} )
     * @Template("user/oubli.html.twig")
     */
    public function oubliAction(Request $request)
    {
        // mot de passe oublié
        $oubli = $this->container->get('user.registration')->oubli($request);
        if ($oubli->isValid()){
            return $this->redirectToRoute('nao_home');
        }
        return array ('form' => $oubli->createView());
    }


    /**
     * @Route("/Inscription", name="registration", schemes={"%la_voie_cryptee%"})
     * @Template("user/registration.html.twig")
     */
    public function registrationAction(Request $request)
    {
        // création d'un compte utilisateur
        $newUser = $this->container->get('user.registration')->regist($request);
        if ($newUser[0]->isValid() && !$newUser[1]){
            return $this->redirectToRoute('active',['essai' => 1]);
        }
        if ($newUser[0]->isValid() && $newUser[1]){
            return $this->redirectToRoute('registration_pro');
        }
        return array ('form' => $newUser[0]->createView(), 'pro' => false );
    }

    /**
     * @Route("/Inscription/Pro", name="registration_pro")
     * @Template("user/registration.html.twig")
     */
    public function registrationProAction(Request $request)
    {
        // complément d'un compte utilisateur Pro
        $newUser = $this->container->get('user.registration')->registPro($request);
        if ($newUser->isValid()){
            return $this->redirectToRoute('active',['essai' => 1 ]);
        }

        return array ('form' => $newUser->createView(), 'pro' => true );
    }

    /**
     * @Route("/useredit", name="user_edit", schemes={"%la_voie_cryptee%"})
     * @Security("has_role('ROLE_USER')")
     * @Template("user/edit.html.twig")
     */
    public function editAction(Request $request)
    {
        // edition d'un compte utilisateur
        $edit = $this->container->get('user.registration')->edit($this->getUser(),$request);
        if ($edit->isValid()){
            return $this->redirectToRoute('nao_home_register');
        }
        return array ('form' => $edit->createView());
    }


    /**
     * @Route("/password", name="password", schemes={"%la_voie_cryptee%"})
     * @Template("user/password.html.twig")
     */
    public function passwordAction(Request $request)
    {
        // changement du mot de passe
        $password = $this->container->get('user.registration')->password($this->getUser(),$request);
        if ($password->isValid()){
            return $this->redirectToRoute('nao_home_register');
        }

        return array ('form' => $password->createView());
    }

    /**
     * @Route("/accord/{code}", name="accord", schemes={"%la_voie_cryptee%"})
     * @Template("user/accord.html.twig")
     */
    public function accordAction($code)
    {
        // accord sur statut Pro
        $reponse = $this->container->get('user.registration')->accord($code);
        return array('msg' =>$reponse[0], 'user' => $reponse[1]);
    }

    /**
     * @Route("/reInit/{code}", name="reInit", schemes={"%la_voie_cryptee%"})
     * @Template("user/password.html.twig")
     */
    public function reInitAction($code, Request $request)
    {
        // Reinitialisation du mot de passe
        $newPassword = $this->container->get('user.registration')->reInit($code, $request);

        if ($newPassword == "pb" || $newPassword->isValid()){
            return $this->redirectToRoute('nao_home');
        }
        return array('form' => $newPassword->createView());
    }
}
