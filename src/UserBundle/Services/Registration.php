<?php
/**
 * Created by PhpStorm.
 * User: Dominique
 * Date: 04/11/2016
 * Time: 17:40
 */

namespace UserBundle\Services;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;
use UserBundle\Form\UserProType;
use UserBundle\Form\UserEditType;
use UserBundle\Form\UserEditProType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use UserBundle\Form\UserChangePasswordType;
use UserBundle\Form\UserInitPasswordType;
use Symfony\Component\HttpFoundation\Request;


class Registration
{
    private $em;
    private $ff;
    private $code;
    private $mail;
    private $mailNao;
    private $template;
    private $voie;

    /**
     * Registration constructor.
     * @param $em
     * @param $ff
     * @param $code
     * @param $mail
     * @param $mailNao
     * @param $template
     * @param $voie
     */
    public function __construct($em, $ff, $code, $mail, $mailNao, $template, $voie)
    {
        // doctrine et form factory
        $this->em = $em;
        $this->ff = $ff;
        $this->code = $code;
        $this->mail = $mail;
        $this->mailNao = $mailNao;
        $this->template = $template;
        $this->voie = $voie;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function regist(Request $request)
    {
        $user = new User();
        $pro = false;
        $form = $this->ff->create(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //cryptage du mot de passe
            $password = $this->code
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $request->getSession()->set('user', $user);

            if ($user->getRole() == 'ROLE_PRO') {
                $pro = true;
            }else{
                $this->envoiMail($user, $request);
            }
        }
        return [$form, $pro, $user];
    }

    /**
     * @param $user
     * @param Request $request
     * @return mixed
     */
    public function registPro(Request $request)
    {
        $user = $request->getSession()->get('user');
        $form = $this->ff->create(UserProType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->envoiMail($user, $request);
            $request->getSession()->set('user', $user);
        }
        return  $form;
    }

    /**
     * @param $essai
     * @param Request $request
     * @return array
     */
    public function activation( $essai, Request $request)
    {
    // saisie du code d'activation
       // $message = array('message' => 'le numéro que vous avez reçu');
        $user = $request->getSession()->get('user');
        $form = $this->ff->createBuilder(FormType::class)
            ->add ('numero', IntegerType::class, array (
                'label' => 'Numéro d\'activation',
                'required' => true))
            ->add('Activer',    SubmitType::class, array(
                'attr' => array('class' =>'btn')))
            ->getForm();
        $form->handleRequest($request);

        $numeroActiv = $request->getSession()->get('numeroActiv');
        if ($form->isSubmitted() && $form->isValid()) {
            $numero = $form->getData();
            if ($numero['numero'] == $numeroActiv){
                $user = $request->getSession()->get('user');
                $user->setIsActive(true);
                if ($user->getRole() == 'ROLE_PRO'){
                    $user->setRole('ROLE_USER');
                    $code = uniqid($user->getPrenom());
                    $user->setCode($code);
                    $this->demandeAccord($user, $code);
                }
                $this->em->persist($user);
                $this->em->flush();
            } else{
                $essai ++;

                $request->getSession()->getFlashBag()->add('info', "Code d'activation erroné");
            }
        }
        return [$form, $user->getIsActive(), $essai] ;
    }

    public function oubli(Request $request)
    {
        $form = $this->ff->createBuilder(FormType::class)
            ->add ('email', EmailType::class, array (
                'label' => 'Votre adresse Email',
                'required' => true,
                'constraints' => new Email (array('message' => 'Cette adresse email n\'est pas valable', 'checkMX' => true ))))
            ->add('Envoyer',    SubmitType::class, array(
                'attr' => array('class' =>'btn')))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->getData('email');
            $user = $this->em->getRepository('UserBundle:User')->findOneBy(['email' => $email]);
           if ($user) {
               $code = uniqid($user->getPrenom());
               $user->setCode($code);
               $this->em->persist($user);
               $this->em->flush();
               $this->envoiOubli($user);
               $request->getSession()->getFlashBag()->add('info', 'Un email vous a été adressé pour la réinitialisation de votre mot de passe');
           } else {
               $request->getSession()->getFlashBag()->add('info', 'l\'adresse email saisie ne correspond à aucun compte existant. Navré!');
           }
        }
        return  $form;
    }


    /**
     * @param $user
     * @param Request $request
     * @return mixed
     */
    public function password($user, Request $request)
    {
        // changement du mot de passe
        $form = $this->ff->create(UserChangePasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //cryptage du mot de passe
            $password = $this->code
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            // enregistrement
            $this->em->persist($user);
            $this->em->flush();
            $request->getSession()->getFlashBag()->add('info', 'Votre mot de passe a bien été modifié');
        }
        return ($form);
    }

    /**
     * @param $user
     * @param Request $request
     * @return mixed
     */
    public function edit($user, Request $request)
    {
        if ($user->getRole() == 'ROLE_PRO'){
            $form = $this->ff->create(UserEditProType::class, $user);
        } else {
            $form = $this->ff->create(UserEditType::class, $user);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // enregistrement
            $this->em->persist($user);
            $this->em->flush();

            $envoi = \Swift_Message::newInstance()->setSubject('Modification de votre compte sur le site de l\'association NAO')
                ->setFrom($this->mailNao)
                ->setTo($user->getEmail())
                ->setBody($this->template->render('user/modif_mail.html.twig',
                    array(  'nom' => $user->getNom(),
                        'prenom' => $user->getPrenom(),
                        'dir_image' =>$this->voie.'://'.$_SERVER['HTTP_HOST'].'/images/logo_min.png'
                    )
                ),'text/html');

            // envoie du mail
            $this->mail->send($envoi);
        }
        return ($form);
    }

    /**
     * @param $user
     * @param $request
     */
    public function envoiMail($user, $request ){
        $numeroActiv = rand(10000, 99999);
        $request->getSession()->set('numeroActiv', $numeroActiv);
        $envoi = \Swift_Message::newInstance()->setSubject('Inscription à l\'application de l\'association NAO')
            ->setFrom($this->mailNao)
            ->setTo($user->getEmail())
            ->setBody($this->template->render('user/active_mail.html.twig',
                array(  'nom' => $user->getNom(),
                        'prenom' => $user->getPrenom(),
                        'numero' => $numeroActiv,
                        'dir_image' =>$this->voie.'://'.$_SERVER['HTTP_HOST'].'/images/logo_min.png'
                )
                ),'text/html');
        // envoie du mail
        $this->mail->send($envoi);
    }

    /**
     * @param $user
     * @param $request
     */
    public function envoiOubli($user ){
        $envoi = \Swift_Message::newInstance()->setSubject('Réinitialisation de votre mot de passe pour l\'application de l\'association NAO')
            ->setFrom($this->mailNao)
            ->setTo($user->getEmail())
            ->setBody($this->template->render('user/oubli_mail.html.twig',
                array(  'nom' => $user->getNom(),
                    'prenom' => $user->getPrenom(),
                    'lien' =>$this->voie.'://'.$_SERVER['HTTP_HOST'].'/reInit/'.$user->getCode(),
                    'dir_image' =>$this->voie.'://'.$_SERVER['HTTP_HOST'].'/images/logo_min.png'
                )
            ),'text/html');
        // envoie du mail
        $this->mail->send($envoi);
    }

    /**
     * @param $user
     * @param $code
     */
    public function demandeAccord($user, $code ){
        $envoi = \Swift_Message::newInstance()->setSubject('Demande de statut professionnel')
            ->setFrom($this->mailNao)
            ->setTo($this->mailNao)
            ->setBody($this->template->render('user/demande_mail.html.twig',
                array(  'nom' => $user->getNom(),
                    'prenom' => $user->getPrenom(),
                    'adresse' => $user->getAdresse(),
                    'specialite' => $user->getSpecialite(),
                    'profession' => $user->getProfession(),
                    'lien' =>$this->voie.'://'.$_SERVER['HTTP_HOST'].'/accord/'.$code
                )
            ),'text/html');
        // envoie du mail
        $this->mail->send($envoi);
    }

    public function reInit($code, $request)
        // Modification du role user en pro et envoi d'un email à l'intéressé
    {
        $user = $this->em->getRepository('UserBundle:User')->findOneBy(['code' => $code]);
        if ($user){
            $form = $this->ff->create(UserInitPasswordType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                //cryptage du mot de passe
                $password = $this->code
                    ->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                // enregistrement
                $user->setCode(null);
                $this->em->persist($user);
                $this->em->flush();
                $request->getSession()->getFlashBag()->add('info', 'Votre mot de passe a bien été réinitialisé, vous pouvez à nouveau vous connecter');
            }
        } else {
            $request->getSession()->getFlashBag()->add('info', 'vous avez soit déjà réinitialisé votre mot de passe, soit il y a comme un problème, et le problème, c\'est peut-être vous... ');
            $form = "pb";
        }
        return $form;
    }

    /**
     * @param $user
     * @param Request $request
     * @return mixed
     */
    public function accord($code)
        // Modification du role user en pro et envoi d'un email à l'intéressé
    {
        $user = $this->em->getRepository('UserBundle:User')->findOneBy(['code' => $code]);
        if ($user){
            $user->setRole('ROLE_PRO');
            $user->setCode(null);
            $msg = 'Vous venez d\'accorder un Statut Professionnel à ';
            $this->em->persist($user);
            $this->em->flush();
            $envoi = \Swift_Message::newInstance()->setSubject('Votre demande de statut professionnel à la NAO')
                ->setFrom($this->mailNao)
                ->setTo($user->getEmail())
                ->setBody($this->template->render('user/confirm_mail.html.twig',
                    array( 'nom' => $user->getNom(),
                        'prenom' => $user->getPrenom(),
                        'dir_image' =>$this->voie.'://'.$_SERVER['HTTP_HOST'].'/images/logo_min.png'
                    )
                ),'text/html');
            // envoie du mail
            $this->mail->send($envoi);

        }else{
            $msg = 'Il y a comme un problème!';
        }
        return [$msg, $user];
    }

}