<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditProType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',        TextType::class, array(
                'label' => 'Nom',
                'constraints' => new Type(array('type' => 'alpha', 'message' => 'Seules des lettres sont autorisées'))
            ))
            ->add('prenom',     TextType::class, array (
                'label' => 'Prénom',
                'constraints' => new Type(array('type' => 'alpha', 'message' => 'Seules des lettres sont autorisées'))
            ))
            ->add('email',      EmailType::class, array(
                'label' => 'Adresse Email',
                'constraints' => new Email (array('message' => 'Cette adresse email n\'est pas valable', 'checkMX' => true )
            )))
            ->add('adresse',     TextType::class, array (
                'label' => 'Adresse',
                'required' => false
            ))

            ->add('specialite',        TextType::class, array(
                'label' => 'Votre spécialité',
                'required' => true
            ))
            ->add('profession',     TextType::class, array (
                'label' => 'Votre activité professionnelle actuelle',
                'required' => true
            ))

            ->add('Valider' ,    SubmitType::class, array (
                'attr' => array('class' =>'btn')
    ))
                    ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_user';
    }


}
