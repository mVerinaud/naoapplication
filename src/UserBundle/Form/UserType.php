<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',        TextType::class, array(
                'label' => 'Nom',
                'constraints' => new Type(array('type' => 'alpha', 'message' => 'Seules des lettres sont autorisées'))
            ))
            ->add('prenom',     TextType::class, array (
                'label' => 'Prénom',
                'constraints' => new Type(array('type' => 'alpha', 'message' => 'Seules des lettres sont autorisées'))
            ))
            ->add('email',      EmailType::class, array(
                'label' => 'Adresse Email',
                'constraints' => new Email (array('message' => 'Cette adresse email n\'est pas valable', 'checkMX' => true )
            )))
            ->add('adresse',     TextType::class, array (
                'label' => 'Adresse',
                'required' => false
            ))

            ->add('username',   TextType::class, array (
                'label' => 'Pseudo',
            ))
            ->add ('role',      ChoiceType::class, array(
                'label' => 'Votre compte : ',
                'choices' =>[
                    'Particulier' => 'ROLE_USER',
                    'Professionnel' => 'ROLE_PRO'],
                'multiple' => false,
                'expanded' => true,
                'attr' => ['class' => 'radio-inline']
            ))
            ->add('plainPassword',   RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Confirmer le mot de passe'),
                'invalid_message' => 'Les deux saisies du mot de passe ne correspondent pas'
            ))

            ->add('Valider' ,    SubmitType::class, array (
                'attr' => array('class' =>'btn')
    ))
                    ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_user';
    }


}
