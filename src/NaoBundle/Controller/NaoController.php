<?php

namespace NaoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

class NaoController extends Controller {

    /**
     * @Route("/", name="nao_home")
     * @Template("nao/accueil.html.twig")
     */
    public function homeAction() {
        // la page d'accueil pour tout visiteur
    }

    /**
     * @Route("/NAO", name="nao_home_register")
     * @Security("has_role('ROLE_USER')")
     * @Template("nao/nao.html.twig")
     */
    public function homeRegisterAction() {
        // la page d'accueil si connecté
        $observations = $this->getDoctrine()->getManager()->getRepository('NaoBundle:Observation')->lastObservationIsValid();
        return ['observations' => $observations];
    }

    /**
     * @Route("/infos", name="nao_waw")s
     * @Template("nao/waw.html.twig")
     */
    public function wawAction() {
        // la page de présentation (who are we...)
    }

    /**
     * @Route("/legal", name="nao_legal")s
     * @Template("nao/legal.html.twig")
     */
    public function legalAction() {
        // la page de mentions légales
    }

    /**
     * @Route("/contact", name="nao_contact")
     * @Template("nao/contact.html.twig")
     */
    public function ContactAction(Request $request) {
        // la page de contact
        $contact = $this->container->get('nao.contact')->contacter($request);
        if ($contact[0]->isValid() && $contact[1] == 1) {
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTIFICATED_FULLY')) {
                return $this->redirectToRoute('nao_home_register');
            } else {
                return $this->redirectToRoute('nao_home');
            }
        }
        return array('form' => $contact[0]->createView(), 'robot' => $contact[1]);
    }
    /**
     * @Route("/faq", name="nao_faq")
     * @Template("nao/faq.html.twig")
     */
    public function FAQAction() {
        // la page de faq (faq)
    }
    
    
    /**
     * @Route("/liste-des-observations", name="nao_list")
     * @Security("has_role('ROLE_USER')")
     * @Template("nao/observations_liste.html.twig")
     */
    public function listAction() {
        // la liste des observations
        $observations = $this->getDoctrine()->getManager()->getRepository('NaoBundle:Observation')->observationIsValid();
        return array('observations' => $observations);
    }

    /**
     * @Route("/mes-observations", name="nao_mylist")
     * @Security("has_role('ROLE_USER')")
     * @Template("nao/observations_user.html.twig")
     */
    public function mylistAction() {
        // mes observations
        $observations = $this->getDoctrine()->getManager()->getRepository('NaoBundle:Observation')->findBy(['user' => $this->getUser()]);
        return array('observations' => $observations);
    }

    /**
     * @Route("/observations-en-attentes", name="nao_valid")
     * @Security("has_role('ROLE_PRO')")
     * @Template("nao/observations_valid.html.twig")
     */
    public function validAction() {
        // observations en attentes
        $observations = $this->getDoctrine()->getManager()->getRepository('NaoBundle:Observation')->observationWait();
        return array('observations' => $observations);
    }

    /**
     *  @Route("/validation-observation/{observationNumber}", requirements={"observationNumber" = "\d+"}, name="nao_validation")
     * @Security("has_role('ROLE_PRO')")
     * @Template("nao/observations_valid.html.twig")
     */
    public function validationAction(Request $request, $observationNumber) {
        // validation des observations
        $this->get('observation.statut')->valideStatut($observationNumber, $request);
        return $this->redirectToRoute('nao_valid');
    }

    /**
     *  @Route("/refus-observation/{observationNumber}", requirements={"observationNumber" = "\d+"}, name="nao_refus")
     * @Security("has_role('ROLE_PRO')")
     * @Template("nao/observations_valid.html.twig")
     */
    public function refusAction(Request $request, $observationNumber) {
        // validation des observations
        $this->get('observation.statut')->refusStatut($observationNumber, $request);
        return $this->redirectToRoute('nao_valid');
    }

    /**
     * @Route("/fiche-observation/{observationNumber}", requirements={"observationNumber" = "\d+"}, name="nao_fiche")
     * @Security("has_role('ROLE_USER')")
     * @Template("nao/observation_fiche.html.twig")
     */
    public function ficheAction($observationNumber) {
        // fiche observation
        $observation = $this->getDoctrine()->getManager()->getRepository('NaoBundle:Observation')->findOneById($observationNumber);
        return array('observations' => $observation);
    }

    /**
     * @Route("/ajouter-une-observation", name="nao_add",  schemes={"%la_voie_cryptee%"})
     * @Security("has_role('ROLE_USER')")
     * @Template("nao/add.html.twig")
     */
    public function addAction(Request $request) {
        // ajout d'une observation
        $observation = $this->get('observation.statut')->addObservation($this->getUser()->getId(), $request);
        if ($observation['send'] == "ok") {
            return $this->redirectToRoute('nao_fiche', array('observationNumber' => $observation['id']));
        } else {
            return array('form' => $observation['form']);
        }
    }

    /**
     * @Route("/especes", name="nao_especes")
     * @Security("has_role('ROLE_USER')")
     * @Template("nao/especes.html.twig")
     */
    public function especesAction()
    {
        //l'affichage des espèces se fait par la requête ajax url especes_paginate
    }

    /**
     * @Route("/paginate", name="especes_paginate")
     * @Security("has_role('ROLE_USER')")
     */
    public function paginateAction(Request $request)
    {
        $output = $this->get('especes.tableau')->requeteEspeces($request);
        return new JsonResponse($output);
    }

    /**
     * @Route("/especes/{especeNumber}", requirements={"especeNumber" = "\d+"}, name="nao_espece_detail")
     * @Security("has_role('ROLE_USER')")
     * @Template("nao/espece_detail.html.twig")
     */
    public function detailEspeceAction($especeNumber) {
        $espece = $this->getDoctrine()->getManager()->getRepository('NaoBundle:Espece')->findOneBy(['cd_nom' => $especeNumber]);
        $observations = $this->getDoctrine()->getManager()->getRepository('NaoBundle:Observation')->observationIsValidByEspeces($especeNumber);

        return array(
            'espece' => $espece,
            'observations' => $observations
        );
    }

    /**
     * @Route("/load", name="load_taxref")
     * @Security("has_role('ROLE_PRO')")
     */
    public function loadTaxrefAction() {
        $this->get('load.taxref')->chargerCSV();
        return $this->redirectToRoute('nao_especes');
    }

}
