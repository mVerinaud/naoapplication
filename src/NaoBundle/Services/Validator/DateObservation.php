<?php

namespace NaoBundle\Services\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateObservation extends Constraint {

    public $message = "%probleme%";

    public function validatedBy() {
        return 'observation_date'; // Ici, on fait appel à l'alias du service
    }

}
