<?php
namespace NaoBundle\Services\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateObservationValidator extends ConstraintValidator {

    public static function afterNow($value) {
        $now = new \DateTime('now');
        if ($value) {
            if ($value->format('Y-m-d') > $now->format('Y-m-d')) {
                return true;
            } else {
               
               return false;
            }

        }
    }
    public static function beforeMaxValue($value){
        $now = new \DateTime('now');
        $datemin=$now->sub(new \DateInterval('P6M'));
                if ($datemin <= $value)
                {return false;}else {return true;}
               
            }
        
    

    public function validate($value, Constraint $constraint) {
        if (self::afterNow($value)) {
             $this->context->buildViolation($constraint->message)->setParameters(array('%probleme%' => 'La date de doit pas être supérieure à la date du jour.'))->addViolation();
    
        }
        if(self::beforeMaxValue($value)){
                $this->context->buildViolation($constraint->message)->setParameters(array('%probleme%' => 'La date de l\'observation ne doit pas avoir plus de 6 mois.'))->addViolation();
     
        }
    }

}
