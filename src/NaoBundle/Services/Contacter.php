<?php
/**
 * Created by PhpStorm.
 * User: Dominique
 * Date: 04/11/2016
 * Time: 17:40
 */

namespace NaoBundle\Services;

use NaoBundle\Entity\Contact;
use NaoBundle\Form\ContactType;
use ReCaptcha\ReCaptcha;
use Symfony\Component\HttpFoundation\Request;

class Contacter
{
    private $em, $ff, $mail, $userToken, $secu,  $mailNao, $cleCaptcha;

    /**
     * Contacter constructor.
     * @param $em
     * @param $ff
     * @param $mail
     * @param $userToken
     * @param $secu
     * @param $mailNao
     */
    public function __construct($em, $ff, $mail, $userToken, $secu, $mailNao, $cleCaptcha)
    {
        $this->em = $em;
        $this->ff = $ff;
        $this->mail = $mail;
        $this->userToken = $userToken;
        $this->secu = $secu;
        $this->mailNao = $mailNao;
        $this->cleCaptcha = $cleCaptcha;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function contacter(Request $request)
    {
        $contact = new Contact();

        $form = $this->ff->create(ContactType::class, $contact);
        $form->handleRequest($request);
        $robot = 0;

        if ($form->isSubmitted() && $form->isValid()) {
            // enregistrement
            if ($this->secu->isGranted('IS_AUTHENTICATED_FULLY')){
                $contact->setNom($this->userToken->getToken()->getUser()->getNom());
                $contact->setPrenom($this->userToken->getToken()->getUser()->getPrenom());
                $contact->setEmail($this->userToken->getToken()->getUser()->getEmail());
                $contact->setAdresse($this->userToken->getToken()->getUser()->getAdresse());
                $this->sendContact($contact);
                $robot = 1;
            } else {
                $recaptcha = new ReCaptcha($this->cleCaptcha );
                $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'));
                if ($resp->isSuccess()) {
                    $this->em->persist($contact);
                    $this->em->flush();
                    $this->sendContact($contact);
                    $robot = 1;
                }else{
                    $robot = 2;
                }
            }

        }
        return [$form, $robot];
    }

    public function sendContact ($contact)
    {
        if($contact->getAdresse()){
            $sujet = 'Message de '. $contact->getNom() .' ' . $contact->getPrenom(). ' de ' . $contact->getAdresse();
        } else{
            $sujet = 'Message de '. $contact->getNom() .' ' . $contact->getPrenom() ;
        }
        $envoi = \Swift_Message::newInstance()->setSubject($sujet)
            ->setFrom($this->mailNao)
            ->setTo($this->mailNao)
            ->setBody($contact->getMessage(). '<br/> Adresse Email : ' . $contact->getEmail(),'text/html');
        // envoie du mail
        $this->mail->send($envoi);

        $envoi = \Swift_Message::newInstance()->setSubject('Copie de votre message adressé à la NAO')
            ->setFrom($this->mailNao)
            ->setTo($contact->getEmail())
            ->setBody($contact->getMessage());
        // envoie du mail
        $this->mail->send($envoi);
    }

}