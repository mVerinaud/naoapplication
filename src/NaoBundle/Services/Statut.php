<?php

namespace NaoBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use NaoBundle\Form\ObservationType;
use NaoBundle\Entity\Observation;
use Symfony\Component\Form\FormFactory;


class Statut {

    private $em;
    private $ff;
    private $upload;

    /**
     * Statut constructor.
     * @param EntityManagerInterface $em
     * @param FormFactory $ff
     * @param Upload $upload
     */
    public function __construct(EntityManagerInterface $em, FormFactory $ff, Upload $upload) {
        $this->em = $em;
        $this->ff = $ff;
        $this->upload = $upload;
    }

    /**
     * @param $user
     * @return string
     */
    public function addStatut($user) {
        if ($user === "ROLE_PRO") {
            return '1';
        } else {
            return '0';
        }
    }

    
    
    public function observationIsWait()
    {
            $observations = $this->em->getRepository('NaoBundle:Observation')->observationWait();
        return $observations;
    }
    
    
    /**
     * @param $observationNumber
     * @param Request $request
     */
    public function valideStatut($observationNumber, Request $request) {
        $observations = $this->em->getRepository('NaoBundle:Observation')->findOneById($observationNumber);
        $observations->setStatut('1');
        $this->em->persist($observations);
        $this->em->flush();
        $request->getSession()->getFlashBag()->add('valid', 'L\'observation a bien été validée. L\'équipe de NAO vous remercie de contribuer à leur programme de recherche.');
    }

    /**
     * @param $observationNumber
     * @param Request $request
     */
    public function refusStatut($observationNumber, Request $request) {
        $observations = $this->em->getRepository('NaoBundle:Observation')->findOneById($observationNumber);
        $observations->setStatut('2');
        $this->em->persist($observations);
        $this->em->flush();
        $request->getSession()->getFlashBag()->add('refus', 'L\'observation a bien été enregistrée comme étant refusée.  L\'équipe de NAO vous remercie de contribuer à leur programme de recherche.');
    }

    /**
     * @param $user_id
     * @param Request $request
     * @return array
     */
    public function addObservation($user_id, Request $request) {
        $user = $this->em->getRepository('UserBundle:User')->findOneById($user_id);
        $observation = new Observation();
        $form = $this->ff->create(ObservationType::class, $observation);
        $form->handleRequest($request);
        if ($form->isValid()) {
            if ($form['photo']->getData() != "") {
                $observation->setPhoto($this->upload->upload($form['photo']->getData()));
            }
            $observation->setUser($user);
            $observation->setStatut($this->addStatut($user->getRole()));
            $this->em->persist($observation);
            $this->em->flush();
            $request->getSession()->getFlashBag()->add('add', 'L\'Observation a bien été enregistrée. L\'équipe de NAO vous remercie de contribuer à leur programme de recherche.');
            return array("send" => "ok", 'id' => $observation->getId());
        } else {
            return array("send" => "non", "form" => $form->createView());
        }
    }

}
