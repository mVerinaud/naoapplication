<?php

namespace NaoBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use NaoBundle\Entity\Espece;

class LoadTaxref
{
    private $em;
    private $habitat1 = "Marin";
    private $habitat2 = "Eau douce";
    private $habitat3 = "Terrestre";
    private $habitat4 = "Marin & Eau douce";
    private $habitat5 = "Marin & terrestre";
    private $habitat6 = "Eau saumâtre";
    private $habitat7 = "Continental (terrestre et/ou eau douce)";
    private $habitat8 = "Continental (terrestre et eau douce)";
    private $rangES = "Espèce";
    private $rangSSES = "Sous-espèce";

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function chargerCSV()
    {
        $csv = fopen(dirname(__FILE__).'/../Resources/Taxref/taxref.csv', 'r');

        $i = 0;
        $batchSize = 20;

        fgetcsv($csv); //ce code permet d'ignorer la ligne de titre du document

        while (!feof($csv))
        {
            $line = fgetcsv($csv, null, ";");
            if ($line === false)
            {
                //la ligne est vide -- Cela permet de ne pas tenir compte de la dernière ligne du csv qui peut être vide
            }
            else
            {
                $line = array_map("utf8_encode", $line);
                $espece = $this->em->getRepository('NaoBundle:Espece')->findOneBy(['cd_nom' => $line[5]]);

                if (!is_object($espece))
                {
                    $espece = new Espece();
                    $espece->setCdNom((int)$line[5]);
                }
                $espece->setCdRef((int)$line[7]);
                $espece->setOrdre($line[3]);
                $espece->setFamille($line[4]);

                switch ($line[8])
                {
                    case "ES":
                        $espece->setRang($this->rangES);
                        break;
                    case "SSES":
                        $espece->setRang($this->rangSSES);
                        break;
                }

                $espece->setNomLatin($line[9]);
                $line[10] == "" ? $espece->setAuteur(null) : $espece->setAuteur($line[10]);
                $espece->setNomLatinComplet($line[12]);
                $line[13] == "" ? $espece->setNomFrancais(null) : $espece->setNomFrancais($line[13]);

                switch ($line[15])
                {
                    case "1":
                        $espece->setHabitat($this->habitat1);
                        break;
                    case "2":
                        $espece->setHabitat($this->habitat2);
                        break;
                    case "3":
                        $espece->setHabitat($this->habitat3);
                        break;
                    case "4":
                        $espece->setHabitat($this->habitat4);
                        break;
                    case "5":
                        $espece->setHabitat($this->habitat5);
                        break;
                    case "6":
                        $espece->setHabitat($this->habitat6);
                        break;
                    case "7":
                        $espece->setHabitat($this->habitat7);
                        break;
                    case "8":
                        $espece->setHabitat($this->habitat8);
                        break;
                    default:
                        $espece->setHabitat($this->habitat8);
                }

                $line[16] == "" ? $espece->setStatusFranceMetropolitaine(null) : $espece->setStatusFranceMetropolitaine($line[16]);
                $line[17] == "" ? $espece->setStatusGuyane(null) : $espece->setStatusGuyane($line[17]);
                $line[18] == "" ? $espece->setStatusMartinique(null) : $espece->setStatusMartinique($line[18]);
                $line[19] == "" ? $espece->setStatusGuadeloupe(null) : $espece->setStatusGuadeloupe($line[19]);
                $line[20] == "" ? $espece->setStatusSaintMartin(null) : $espece->setStatusSaintMartin($line[20]);
                $line[21] == "" ? $espece->setStatusSaintBarthelemy(null) : $espece->setStatusSaintBarthelemy($line[21]);
                $line[22] == "" ? $espece->setStatusSaintPierreEtMiquelon(null) : $espece->setStatusSaintPierreEtMiquelon($line[22]);
                $line[23] == "" ? $espece->setStatusMayotte(null) : $espece->setStatusMayotte($line[23]);
                $line[24] == "" ? $espece->setStatusIlesEparses(null) : $espece->setStatusIlesEparses($line[24]);
                $line[25] == "" ? $espece->setStatusReunion(null) : $espece->setStatusReunion($line[25]);
                $line[26] == "" ? $espece->setStatusTAAF(null) : $espece->setStatusTAAF($line[26]);
                $line[29] == "" ? $espece->setStatusPolynesieFrancaise(null) : $espece->setStatusPolynesieFrancaise($line[29]);
                $line[27] == "" ? $espece->setStatusNouvelleCaledonie(null) : $espece->setStatusNouvelleCaledonie($line[27]);
                $line[28] == "" ? $espece->setStatusWallisEtFutuna(null) : $espece->setStatusWallisEtFutuna($line[28]);
                $line[30] == "" ? $espece->setStatusClipperton(null) : $espece->setStatusClipperton($line[30]);

                $this->em->persist($espece);

                if ($i % $batchSize === 0)
                {
                    $this->em->flush();
                    $this->em->clear();
                }

                $i++;
            }
        }

        fclose($csv);
        $this->em->flush();
        $this->em->clear();
    }

}