<?php

namespace NaoBundle\Services;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use NaoBundle\Entity\Espece;

class AffichageEspeces
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function requeteEspeces(Request $request)
    {
        $length = $request->get('length');
        $length = $length && ($length!=-1)?$length:0;

        $start = $request->get('start');
        $start = $length?($start && ($start!=-1)?$start:0)/$length:0;

        $search = $request->get('search');
        $sort = $request->get('order');

        $filters = [
            'query' => $search['value'],
            'colonne' => $sort[0]['column'],
            'orderDirection' => $sort[0]['dir']
        ];

        $especes = $this->em->getRepository('NaoBundle:Espece')->search(
            $filters, $start, $length
        );

        $output = array(
            'data' => array(),
            'recordsFiltered' => count($this->em->getRepository('NaoBundle:Espece')->search($filters, 0, false)),
            'recordsTotal' => count($this->em->getRepository('NaoBundle:Espece')->search(array(), 0, false))
        );

        foreach ($especes as $espece)
        {
            $output['data'][] = [
                'famille' => $espece->getFamille(),
                'rang' => $espece->getRang(),
                'nomLatin' => $espece->getNomLatin(),
                'nomFrancais' => $espece->getNomFrancais(),
                'habitat' => $espece->getHabitat(),
                'cdNom' => "<a href='/especes/".$espece->getCdNom()."'>Voir la fiche détaillée</a>",
            ];
        }

        return $output;
    }

}