<?php

namespace NaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use NaoBundle\Services\Validator\DateObservation;
/**
 * Observation
 *
 * @ORM\Table(name="observation")
 * @ORM\Entity(repositoryClass="NaoBundle\Repository\ObservationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Observation {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NaoBundle\Entity\Espece", cascade={"persist"}, inversedBy="observation")
     * @ORM\JoinColumn(name="espece_id", referencedColumnName="id") 
     */
    private $especes;

       /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", cascade={"persist"}, inversedBy="observation")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id") 
     */
    private $user;
    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     * @DateObservation{message=""}
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="coordonnees", type="string", length=255)
     * 
     */
    private $coordonnees;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;
     
     /**
     * @var int
     *
     * @ORM\Column(name="statut", type="integer")
     */   
    private $statut;
    public function __construct() {
        $this->espece = new ArrayCollection();
        $this->statut = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Observation
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Observation
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set coordonnees
     *
     * @param string $coordonnees
     *
     * @return Observation
     */
    public function setCoordonnees($coordonnees) {
        $this->coordonnees = $coordonnees;

        return $this;
    }

    /**
     * Get coordonnees
     *
     * @return string
     */
    public function getCoordonnees() {
        return $this->coordonnees;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Observation
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Observation
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Set cp
     *
     * @param string $cp
     *
     * @return Observation
     */
    public function setCp($cp) {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp() {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Observation
     */
    public function setVille($ville) {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * Set especes
     *
     * @param \NaoBundle\Entity\Espece $especes
     *
     * @return Observation
     */
    public function setEspeces(\NaoBundle\Entity\Espece $especes) {
        $this->especes = $especes;

        return $this;
    }

    /**
     * Get especes
     *
     * @return \NaoBundle\Entity\Espece
     */
    public function getEspeces() {
        return $this->especes;
    }

    /**
     * Set espece
     *
     * @param \NaoBundle\Entity\Espece $espece
     *
     * @return Observation
     */
    public function setEspece(\NaoBundle\Entity\Espece $espece) {
        $this->espece = $espece;

        return $this;
    }

    /**
     * Get espece
     *
     * @return \NaoBundle\Entity\Espece
     */
    public function getEspece() {
        return $this->espece;
    }

    /**
     * Set cdNom
     *
     * @param \NaoBundle\Entity\Espece $cdNom
     *
     * @return Observation
     */
    public function setCdNom(\NaoBundle\Entity\Espece $cdNom = null) {
        $this->cd_nom = $cdNom;

        return $this;
    }

    /**
     * Get cdNom
     *
     * @return \NaoBundle\Entity\Espece
     */
    public function getCdNom() {
        return $this->cd_nom;
    }

    /**
     * Set idEspeces
     *
     * @param \NaoBundle\Entity\Espece $idEspeces
     *
     * @return Observation
     */
    public function setIdEspeces(\NaoBundle\Entity\Espece $idEspeces = null) {
        $this->id_especes = $idEspeces;

        return $this;
    }

    /**
     * Get idEspeces
     *
     * @return \NaoBundle\Entity\Espece
     */
    public function getIdEspeces() {
        return $this->id_especes;
    }


    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Observation
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set statut
     *
     * @param boolean $statut
     *
     * @return Observation
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return boolean
     */
    public function getStatut()
    {
        return $this->statut;
    }
}
