<?php

// src/NaoBundle/Form/Type/ObservationType.php

namespace NaoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObservationType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('especes', EntityType::class, array(
                    'class' => 'NaoBundle:Espece',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')->where('u.nomFrancais IS NOT NULL')
                                ->groupBy('u.nomFrancais')
                                ->orderBy('u.nomFrancais', 'ASC');
                    },
                    'choice_label' => 'nom_francais',
                    'placeholder' => 'Choisissez un nom d\'oiseau',
                ))
                ->add('date', DateType::class, array('widget' => 'single_text',
                    'html5' => false,
                    'attr' => ['class' => 'js-datepicker'],
                    'format' => 'dd/MM/yyyy',
                    'input' => 'datetime',
                ))
                ->add('coordonnees', TextType::class)
                ->add('adresse', TextType::class)
                ->add('photo', FileType::class, array('required' => false))
                ->add('Valider', SubmitType::class);
        $builder->getForm();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'NaoBundle\Entity\Observation',
        ));
    }

}
