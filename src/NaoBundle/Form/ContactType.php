<?php

namespace NaoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Type;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom' ,   TextType::class, array(
                'label' => 'Nom',
                'constraints' => new Type(array('type' => 'alpha', 'message' => 'Seules des lettres sont autorisées'))
            ))
            ->add('prenom', TextType::class, array(
                'label' => 'Prénom',
                'constraints' => new Type(array('type' => 'alpha', 'message' => 'Seules des lettres sont autorisées'))
            ))
            ->add('email',  EmailType::class, array(
                'label' => 'Adresse Email',
                'constraints' => new Email (array('message' => 'Cette adresse email n\'est pas valable', 'checkMX' => true )
    )))
            ->add('adresse' , TextType::class, array(
                'label' => 'Lieu de résidence',
                'required' => false
            ) )
            ->add('message' , TextareaType::class, array(
                'label' => 'Votre message'
            ))

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NaoBundle\Entity\Contact'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'naobundle_contact';
    }


}
