<?php

namespace NaoBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EspeceRepository extends EntityRepository
{
    public function search($data, $page = 0, $max = NULL, $getResult = true)
    {
        $qb = $this->createQueryBuilder('especes');
        $query = isset($data['query']) && $data['query']?$data['query']:null;
        if (isset($data['colonne']))
        {
            $sort = $data['colonne'];
        }
        else
        {
            $sort = null;
        }

        if ($query)
        {
            $qb
                ->andWhere('especes.nomFrancais like :query')
                ->orWhere('especes.nomLatin like :query')
                ->orWhere('especes.habitat like :query')
                ->orWhere('especes.rang like :query')
                ->orWhere('especes.famille like :query')
                ->setParameter('query', "%".$query."%");
        }

        if (($sort != null) && ($data['orderDirection'] === 'asc' || $data['orderDirection'] === 'desc'))
        {
            switch ($sort)
            {
                case 0 :
                {
                    $qb
                        ->orderBy('especes.famille', strtoupper($data['orderDirection']));
                    break;
                }
                case 1 :
                {
                    $qb
                        ->orderBy('especes.rang', strtoupper($data['orderDirection']));
                    break;
                }
                case 2 :
                {
                    $qb
                        ->orderBy('especes.nomLatin', strtoupper($data['orderDirection']));
                    break;
                }
                case 3 :
                {
                    $qb
                        ->orderBy('especes.nomFrancais', strtoupper($data['orderDirection']));
                    break;
                }
                case 4 :
                {
                    $qb
                        ->orderBy('especes.habitat', strtoupper($data['orderDirection']));
                    break;
                }
            }
        }

        if ($max)
        {
            $preparedQuery = $qb->getQuery()
                ->setMaxResults($max)
                ->setFirstResult($page * $max);
        }
        else
        {
            $preparedQuery = $qb->getQuery();
        }

        return $getResult?$preparedQuery->getResult():$preparedQuery;
    }

}